#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <math.h>
#include <x86intrin.h>
#include <sys/time.h>
#define _mm_srli_epi8(mm, Imm) _mm_and_si128(_mm_set1_epi8(0xFF >> Imm), _mm_srli_epi32(mm, Imm))

using std::cout;
using std::endl;
using namespace cv;

int main()
{
    struct timeval startS, endS, startV, endV;
    cout << "810196684, 810196683" << endl;
    Mat img1 = imread("sardar.png", 0);
    Mat img2 = imread("ut.png", 0);
    // int thresh = 127;
    // (thresh, img2) = threshold(img2, 128, 255, THRESH_BINARY | THRESH_OTSU);

    unsigned char *in_image1;
    unsigned char *in_image2;
    in_image1 = (unsigned char *) img1.data;
    in_image2 = (unsigned char *) img2.data;
    double alpha = 0.5;

    gettimeofday(&startS, NULL);
    for (int row = 0; row < img2.rows; row++) {
        for (int col = 0; col < img2.cols; col++) {
            (*(in_image1 + row * img1.cols + col)) = min(255.0, ((*(in_image1 + row * img1.cols + col)) + ((*(in_image2 + row * img2.cols + col))*alpha)));
        }  
    }
	gettimeofday(&endS, NULL);

    __m128i *pSrc1;
    __m128i *pSrc2;
    __m128i m1, m2, m3, m5;
    Mat img1p = imread("sardar.png", 0);
    pSrc1 = (__m128i *) img1p.data;
    pSrc2 = (__m128i *) img2.data;

    gettimeofday(&startV, NULL);
    
    for (int i = 0; i < img2.rows; i++)
        for (int j = 0; j < img2.cols / 16; j++) {
            m1 = _mm_loadu_si128(pSrc1 + i * img1.cols/16 + j);
            m2 = _mm_loadu_si128(pSrc2 + i * img2.cols/16 + j);
            m5 = _mm_srli_epi8(m2, 1);
            m3 =  _mm_adds_epu8(m1, m5);
            _mm_storeu_si128(pSrc1 + i * img1.cols/16 + j, m3);
        }
	gettimeofday(&endV, NULL);

    long secondsS = (endS.tv_sec - startS.tv_sec);
	long microsS = ((secondsS * 1000000) + endS.tv_usec) - (startS.tv_usec);
    
    long secondsV = (endV.tv_sec - startV.tv_sec);
	long microsV = ((secondsV * 1000000) + endV.tv_usec) - (startV.tv_usec);
    
    printf("Serial Run time = %ld sec and %ld mSec \n", secondsS, microsS);
	printf("Parallel Run time = %ld sec and %ld mSec \n", secondsV, microsV);
	printf("Speedup = %f\n\n", (float) microsS/(float) microsV);

    namedWindow("Question2 serial result");
    namedWindow("Question2 parallel result");
    imshow("Question2 parallel result", img1p);
    imshow("Question2 serial result", img1);

    waitKey(0);
    return 0;
}