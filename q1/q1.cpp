#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <math.h>
#include <x86intrin.h>
#include <sys/time.h>

using std::cout;
using std::endl;
using namespace cv;

int main()
{
    struct timeval startS, endS, startV, endV;
    cout << "810196684, 810196683" << endl;
    Mat img1 = imread("1.png", 0);
    Mat img2 = imread("2.png", 0);
    
    Mat out_img (img1.rows, img1.cols, CV_8U);

    unsigned char *in_image1;
    unsigned char *in_image2;
    unsigned char *out_image;
    in_image1 = (unsigned char *) img1.data;
    in_image2 = (unsigned char *) img2.data;
    out_image = (unsigned char *) out_img.data;

    gettimeofday(&startS, NULL);
    for (int row = 0; row < img1.rows; row++) {
        for (int col = 0; col < img1.cols; col++) {
            (*(out_image + row * img1.cols + col)) = abs((*(in_image1 + row * img1.cols + col)) - (*(in_image2 + row * img1.cols + col)));
        }  
    }
	gettimeofday(&endS, NULL);   

    __m128i *pSrc1;
    __m128i *pSrc2;
    __m128i *pRes;
    __m128i m1, m2, m3;
    Mat out_img2 (img1.rows, img1.cols, CV_8U);
    unsigned char *out_image2;
    pSrc1 = (__m128i *) img1.data;
    pSrc2 = (__m128i *) img2.data;
    pRes = (__m128i *) out_img2.data;

    gettimeofday(&startV, NULL);
    for (int i = 0; i < img1.rows; i++)
        for (int j = 0; j < img1.cols / 16; j++) {
            m1 = _mm_loadu_si128(pSrc1 + i * img1.cols/16 + j) ;
            m2 = _mm_loadu_si128(pSrc2 + i * img1.cols/16 + j) ;
            m3 =  _mm_or_si128(_mm_subs_epu8(m1, m2), _mm_subs_epu8(m2, m1));
            _mm_storeu_si128(pRes + i * img1.cols/16 + j, m3);
        }
	gettimeofday(&endV, NULL);

    long secondsS = (endS.tv_sec - startS.tv_sec);
	long microsS = ((secondsS * 1000000) + endS.tv_usec) - (startS.tv_usec);
    
    long secondsV = (endV.tv_sec - startV.tv_sec);
	long microsV = ((secondsV * 1000000) + endV.tv_usec) - (startV.tv_usec);

    printf("Serial Run time = %ld sec and %ld mSec \n", secondsS, microsS);
	printf("Parallel Run time = %ld sec and %ld mSec \n", secondsV, microsV);
	printf("Speedup = %f\n\n", (float) microsS/(float) microsV);

    namedWindow("Question1 serial result");
    imshow("Question1 serial result", out_img);

    namedWindow("Question1 parallel result");
    imshow("Question1 parallel result", out_img2);

    waitKey(0);
    return 0;
}

